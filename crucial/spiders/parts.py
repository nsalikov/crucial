# -*- coding: utf-8 -*-
import re
import json
import scrapy
import js2xml
import lxml.etree
from jsonfinder import jsonfinder
from scrapy.shell import inspect_response
from urllib.parse import urlparse, urlunparse, parse_qsl, urlencode


class PartsSpider(scrapy.Spider):
    name = 'parts'
    allowed_domains = ['crucial.com']
    start_urls = ['https://www.crucial.com/upgrades']

    def parse(self, response):
        for url in response.css('#oemdisplay a'):
            link = url.css('a ::attr(href)').extract_first()
            manufacturer = url.css('a ::text').extract_first()
            meta = {'manufacturer': manufacturer}

            if link:
                yield response.follow(link, meta=meta, callback=self.parse_line)


    def parse_line(self, response):
        for url in response.css('#oemdisplay a'):
            link = url.css('a ::attr(href)').extract_first()
            line = url.css('a ::text').extract_first()
            meta = {
                    'manufacturer': response.meta['manufacturer'],
                    'line': line
            }

            if link:
                yield response.follow(link, meta=meta, callback=self.parse_models)


    def parse_models(self, response):
        for url in response.css('#oemdisplay a'):
            link = url.css('a ::attr(href)').extract_first()
            model = url.css('a ::text').extract_first()
            meta = {
                    'manufacturer': response.meta['manufacturer'],
                    'line': response.meta['line'],
                    'model': model
            }

            if link:
                yield response.follow(link, meta=meta, callback=self.parse_model)


    def parse_model(self, response):
        d = {}
        d['manufacturer'] = response.meta['manufacturer']
        d['line'] = response.meta['line']
        d['model'] = response.meta['model']
        d['url'] = response.url
        d['specs'] = dict(self.get_specs(response))
        d['parts'] = list(self.get_parts(response))

        yield d


    def get_specs(self, response):
        p_css = '.ss-results .ss-results-detail .ss-results-detail-content p:not(.text-small) ::text'

        for p in response.css(p_css).extract():

            key, value = p.split(':')
            key = key.strip()
            value = value.strip()

            yield key, value


    def get_parts(self, response):
        scripts_css = 'script ::text'
        scripts = [s for s in response.css(scripts_css).extract() if 'prodList' in s]

        for script in scripts:
            try:
                xml = lxml.etree.tostring(js2xml.parse(script), encoding='unicode')
                js = list(jsonfinder(xml))
                parts_list = js[1][2]
            except:
                continue

            for p in parts_list:
                yield self.fix_part(p)


    def fix_part(self, part):
        return part
